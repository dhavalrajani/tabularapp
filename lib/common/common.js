FcCollections = {};
FcSchemas = {};
SmtAdminTabular = {};
PAGE_LIMIT = 10;
SmtAdminTabular.tableRecords = new Mongo.Collection('FcTabular_records');

SmtAdminTabular.getRecord = function(name) {
    return SmtAdminTabular.tableRecords.findOne(name);
};
