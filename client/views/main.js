Template.main.onCreated(function (){
    this.filterQuery = new ReactiveDict();
    this.filterQuery.set("query",{});
    var self = this;
    this.autorun(function () {
        var query = {};
        if(Router.current() && Router.current().params && Router.current().params.query && Router.current().params.query.filter){
            query = {"username":{$regex: Router.current().params.query.username, $options: 'i'}};

        }
        self.filterQuery.set("query",query);
    });

});

Template.main.helpers({
    selector: function () {
        return Template.instance().filterQuery.get("query");
    },
    getQueryParams: function (field) {
        if(Router.current() && Router.current().params && Router.current().params.query && Router.current().params.query.filter){
            var query = Router.current().params.query;
            return query[field];
        }
    }
});

Template.main.events({
    "click #search": function (e,t) {
        e.preventDefault();
        var username = $("[name='username']").val();
        var existingQuery = {};
        if(Router.current() && Router.current().params && Router.current().params.query){
            existingQuery = Router.current().params.query;
        }
        if(username != ""){
            _.extend(existingQuery,{filter:1,username:username});
            Router.go(Router.current().route.getName(),{},{query:existingQuery})
        }else{
            delete existingQuery.filter;
            delete existingQuery.username;
            Router.go(Router.current().route.getName(),{},{query:existingQuery})
        }
    }
})