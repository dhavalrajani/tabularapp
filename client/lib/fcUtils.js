PAGE_LIMIT = 10;
PAGE_LIMIT_MR =10;
this.FcUtils = function () {
    var FcUtilsClass = function () {

    };
    FcUtilsClass.prototype.showMoreButtonVisible = function (opts, limit, selectorClass,templateInstance) {
        var force = opts.force || false;
        var threshold, target = $(selectorClass);
        if (!target.length) return;
        threshold = $(window).scrollTop() + $(window).height() - target.height();
        if (force || target.offset().top < threshold + 1) {
            $(selectorClass).find("button").attr("disabled", true);
            templateInstance.pageLimit.set("count", templateInstance.pageLimit.get("count") + PAGE_LIMIT);
        }
    };

    FcUtilsClass.prototype.stickFooterAtBottom = function () {
        $('.smart_admin .main_menu').height($(window).height()-$('.smart_admin .header').outerHeight(true)-$('.smart_admin .footer').outerHeight(true)-1).css({});
        $('.smart_admin .swiper_tab_content').height($(window).height()-$('.smart_admin .header').outerHeight(true)-$('.smart_admin .swiper_tabs').outerHeight(true)-$('.smart_admin .Calphabets').outerHeight(true)-$('.smart_admin .footer').outerHeight(true)-18).css({'overflow':'auto'});
        $('.smart_admin .main_content_div.with_no_tabs').height($(window).height()-$('.smart_admin .header').outerHeight(true)-$('.smart_admin .swiper_tabs').outerHeight(true)-$('.smart_admin .Calphabets').outerHeight(true)-$('.smart_admin .footer').outerHeight(true)-18).css({'overflow':'auto'});
    };

    FcUtilsClass.prototype.updateFloatingLabel = function () {
        setInterval(function () {
            $('.floating-labe1 .form-control').each(function () {
                if( $(this).val() && $(this).val()!= "" && $(this).val().length > 0){
                    $(this).parents('.form-group').toggleClass('focused', true);
                }
            })
        })
        $('.floating-labe1 .form-control').on('focus blur', function (e) {
            $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus'));
        }).trigger('blur');

        $('.floating-labe1 .form-control').focus(function () {
            $(this).data('placeholder', $(this).attr('placeholder'))
                .attr('placeholder', '');
        }).blur(function () {
            $(this).attr('placeholder', $(this).data('placeholder'));
        });
    };

    FcUtilsClass.prototype.swiperTab = function (cb) {
        var mySwiper = new Swiper('.swiper_tabs,.Calphabets,.global_customer_tabs', {
            speed: 400,
            slidesPerView: 'auto',
            loop: false,
            spaceBetween: 1
        });
        var RM = new Swiper('.RM', {
            pagination: '.swiper-pagination1',
            paginationClickable: true,
            spaceBetween: 30
        });

        if(cb){
            return cb(mySwiper);
        }
    };
    FcUtilsClass.prototype.attachSelect2 = function (element,options) {
        if(options){
            element.select2(options)
        }else{
            element.select2({});
        }

        $(element).on("select2-open", function(event){
            var container = $(".smtPicker-attached");// time picker
            _.each(container,function(){
                $(container).smtTimePicker('remove')
            })
            $(".datepicker").hide();
        });
    };

    FcUtilsClass.prototype.getFormValues = function (formObject, submitCallback) {

            var values = {};
            formObject.find("input,select,textarea").each(function () {
                var inputObject = $(this);
                var fieldName = inputObject.attr("name");
                var fieldValue = $.trim(inputObject.val());
                if (inputObject.attr("type") == "checkbox") {
                    // auto set data type for checkbox
                    if (!inputObject.attr("data-type")) {

                        // single checkbox with that name means dataType="BOOL" else it is "ARRAY"
                        if (formObject.find("input[name='" + fieldName + "']").length == 1 && !inputObject.attr("data-custom")) {
                            dataType = "BOOL";
                        }
                        else {
                            dataType = "ARRAY";
                        }
                    }
                    if (dataType == "BOOL") fieldValue = inputObject.is(":checked");
                    if (dataType == "ARRAY") fieldValue = inputObject.is(":checked") ? fieldValue : "off";
                    if (dataType == "ARRAY") {
                        if ($.isArray(values[fieldName]) && values[fieldName].length != 0) {
                            if (fieldValue != "") {
                                values[fieldName].push(fieldValue)
                            }
                        } else {
                            values[fieldName] = [];
                            if (fieldValue != "") {
                                values[fieldName].push(fieldValue)
                            }

                        }
                    } else {
                        values[fieldName] = fieldValue;
                    }

                } else if (inputObject.attr("type") == "radio") {
                    if (inputObject.is(":checked")) {
                        values[fieldName] = $.trim(fieldValue);
                    }
                } else {
                    if(!inputObject.attr("data-custom") ){
                        values[fieldName] = $.trim(fieldValue);
                    }else{
                        dataTypeInput = "ARRAY";
                        if(dataTypeInput == "ARRAY"){
                            if ($.isArray(values[fieldName]) && values[fieldName].length != 0) {
                                values[fieldName].push($.trim(fieldValue))

                            } else {
                                values[fieldName] = [];

                                values[fieldName].push($.trim(fieldValue))


                            }
                        }else{
                            values[fieldName] = $.trim(fieldValue);
                        }
                    }

                }
            });
            if (submitCallback) {
                submitCallback(values);
            }


    };
    FcUtilsClass.prototype.removedatepicker = function (element) {
        element.datepicker('remove');
    };
    FcUtilsClass.prototype.attachdatepicker = function (element,config) {
        if(config && config.format){
            // config.format = dateFormatIntoBootsTrapFormat(config.format)
        }
        else{
            var format = getCompanyDateFormat();
            config.format = dateFormatIntoBootsTrapFormat(format);
        }
        element.datepicker(config).on('hide.bs.modal', function(event) {
            event.stopPropagation();
        }).on("show.bs.modal", function (event) {
            event.stopPropagation();
        }).on("hide", function(e) {
            var element = $(this);
            if($(this).find("input").length > 0){
                element = $(this).find("input");
            }
            $(this).datepicker('setDate', element.val());
        });
        // [Template.instance().view.name].onDestroyed(function(){debugger})

    };
    FcUtilsClass.prototype.attachtimepicker = function (element,config) {
        element.datetimepicker(config).on('hide.bs.modal', function(event) {
            event.stopPropagation();
        }).on("show.bs.modal", function (event) {
            event.stopPropagation();
        }).on("hide", function(e) {
            $(this).datetimepicker('setDate', $(this).val());
        });
    };
    FcUtilsClass.prototype.mytest = function (o) {
        var oo = {}, t, parts, part;
        for (var k in o) {
            t = oo;
            parts = k.split('.');
            var key = parts.pop();
            while (parts.length) {
                part = parts.shift();
                t = t[part] = t[part] || {};
            }
            t[key] = o[k]
        }
        return oo;
    };

    FcUtilsClass.prototype.getFormValuesBySchema = function (formObject, submitCallback) {
        var values = [];
        formObject.find("input,select,textarea").each(function () {
            var inputObject = $(this);
            var fieldName = inputObject.attr("name");
            var fieldValue = $.trim(inputObject.val());
            if (inputObject.attr("type") == "checkbox") {
                // auto set data type for checkbox
                if (!inputObject.attr("data-type")) {

                    // single checkbox with that name means dataType="BOOL" else it is "ARRAY"
                    if (formObject.find("input[name='" + fieldName + "']").length == 1 && !inputObject.attr("data-custom")) {
                        dataType = "BOOL";
                    }
                    else {
                        dataType = "ARRAY";
                    }
                }
                if (dataType == "BOOL") fieldValue = inputObject.is(":checked");
                if (dataType == "ARRAY") fieldValue = inputObject.is(":checked") ? fieldValue : "off";
                if (dataType == "ARRAY") {
                    if ($.isArray(values[fieldName]) && values[fieldName].length != 0) {
                        if (fieldValue != "") {
                            values[fieldName].push(fieldValue)
                        }
                    } else {
                        values[fieldName] = [];
                        if (fieldValue != "") {
                            values[fieldName].push(fieldValue)
                        }

                    }
                } else {
                    values[fieldName] = fieldValue;
                }

            } else if (inputObject.attr("type") == "radio") {
                if (inputObject.is(":checked")) {
                    values[fieldName] = $.trim(fieldValue);
                }
            } else {
                if(!inputObject.attr("data-custom") ){
                    values[fieldName] = $.trim(fieldValue);
                }else{
                    dataTypeInput = "ARRAY";
                    if(dataTypeInput == "ARRAY"){
                        if ($.isArray(values[fieldName]) && values[fieldName].length != 0) {
                            values[fieldName].push($.trim(fieldValue))

                        } else {
                            values[fieldName] = [];

                            values[fieldName].push($.trim(fieldValue))


                        }
                    }else{
                        values[fieldName] = $.trim(fieldValue);
                    }
                }

            }
        });
        if (submitCallback) {
            submitCallback(this.mytest(values));
        }

    }

    FcUtilsClass.prototype.mrSwiperTab = function () {
        var swiperLib = new Swiper('.swiperLib', {
            pagination: '.swiper-pagination1',
            slidesPerView: 10,
            paginationClickable: true,
            spaceBetween: 5,
        });
        var swiperAlphas = new Swiper('.swiperAlphas', {
            scrollbar: $(this).find('.swiper-scrollbar')[0],
            direction: 'vertical',
            slidesPerView: 20,
            mousewheelControl: true,
            freeMode: true
        })
    };

    FcUtilsClass.prototype.closePopup = function(triggerElement,popUpelement,selector) {
        $(document).on("click", function(event){
            var $trigger = triggerElement;
            if($trigger[0] !== event.target && !$trigger.has(event.target).length && !popUpelement.has(event.target).length){
                popUpelement.hide();
            }
        });

        $("select").select2().on('select2-focus', function(){
            popUpelement.hide();
        })

    }

    FcUtilsClass.prototype.attachSmtTimePicker = function(element,config) {
        if(config.format == undefined || config.format != ""){
            config.format = getCompanyTimeFormat();
        }
        $(element).smtTimePicker(config)

    }

    FcUtilsClass.prototype.attachSmtDurationPicker = function(element,config) {
        $(element).smtTimePicker(config);
    }
    //-----START:Deprecated------
    //Use writeToExternalStorage instead of writeToFile for better persistence
    //use writeToFile to write in device internal storage
    var tempFS ;
    FcUtilsClass.prototype.writeToFile = function (url) {
        window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;

        var requestedBytes = 1024*1024*200; // 200MB
        try {
            navigator.webkitPersistentStorage.requestQuota(
                requestedBytes, function (grantedBytes) {
                    window.requestFileSystem(window.PERSISTENT, grantedBytes, function (fs) {
                        tempFS = fs;
                        console.log('file system open: ' + fs.name);
                        getSampleFile(fs.root, url);

                    }, function (err) {
                        console.log(err);
                    });
                }, function (e) {
                    console.log('Error', e);
                }
            );
        }catch (exception){
            toastr.error("Download is not possible");
        }
    }

    //---END:Deprecated------

    return FcUtilsClass;
}();
