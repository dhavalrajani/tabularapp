Template.registerHelper('selected', function (desiredValue, itemValue) {
    if (desiredValue == undefined && itemValue == undefined) {
        return ""
    }

    if (_.isArray(desiredValue)) {
        return desiredValue.indexOf(itemValue) >= 0 ? "selected" : "";
    }
    return desiredValue == itemValue ? "selected" : "";
});