FcTableTabular = {};
FcTableTabular.userData = function () {
    return {
        order: [0, 'desc'],
        collectionName: "FcCollections.Users",
        name: "userData",
        blankStateMessage: "No Products found",
        tableViewConfig: {
            columns: [
              {data: "createdAt", Title: "Created At",sort:false,render: function (val, type, doc) {
                  return moment(doc.createdAt).format("DD-MM-YYYY");
              }
            },
                {data: "_id", Title: "Id",sort:true},
                {data: "username", Title: "Username",sort:true},

            ]
        }
    }
};
FcTableTabular.UIUserData = function () {
    return {
        name: "UIUserData",
        tableViewConfig: {
            columns: [
                {data: "createdAt", Title: "Created At",sort:true,render: function (val, type, doc) {
                    return moment(doc.createdAt).format("DD-MM-YYYY");
                }
                },
                {data: "_id", Title: "Id",sort:true},
                {data: "username", Title: "Username",sort:true},

            ]
        }
    }
};
Template.registerHelper("FcTableTabular", FcTableTabular);
