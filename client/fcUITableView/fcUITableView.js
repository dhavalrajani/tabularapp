Template.FcUITableView.onCreated(function () {
    var template = this;
    var records = {};

    template.pageLimit = new ReactiveDict();
    template.pageLimit.set("count",PAGE_LIMIT);
    template.skipCount = new ReactiveDict();
    template.skipCount.set("count",0);
    template.selector = new ReactiveDict();
    template.sort = new ReactiveDict();
    template.tableData = new ReactiveDict();
    template.tableData.set("data",[]);
    var data;
    template.autorun(function () {
        var tableData = Template.currentData().tableData;
        if(tableData){
            template.tableData.set("data",tableData);
        }
        data = Template.currentData().table;
        var selector = Template.currentData().selector;
        template.selector.set("selectorQuery",selector);
    });
    var collectionName = this.data.table.collectionName;
    var tableName = this.data.table.name;
    var fields = {};
    var sort = null;


    _.each(data.columns, function (col) {
        if(col.data){
            var cleanFieldName = new FcUtils().cleanFieldName(col.data);
            fields[cleanFieldName] = 1;
        }

    });

    _.each(data["extraFields"],function (value1,index1){
        var cleanExtraFieldName = new FcUtils().cleanFieldName(value1);
        fields[cleanExtraFieldName] = 1;
    })

    _.extend(fields,{_id:1});
    /*var publicationName = this.data.table.pub || "fcTabularView_genericPub";
    template.autorun(function () {
      if(Router.current() && Router.current().params && Router.current().params.query && Router.current().params.query.sort){
        var order = Router.current().params.query.order == "desc" ? -1 : 1;
        sort = {};
        sort[Router.current().params.query.sort] = order;
        template.sort.set("data",{column:Router.current().params.query.sort,"order":order});
      }else{
        sort = {};
        if(data.order && data.order.length > 0){
          var order = data.order[1] == "desc"? -1 : 1;
          if(data.tableViewConfig.columns && data.tableViewConfig.columns.length >0  && data.tableViewConfig.columns[data.order[0]] && data.tableViewConfig.columns[data.order[0]].data){
              sort[new FcUtils().cleanFieldName(data.tableViewConfig.columns[data.order[0]].data)] = order;
              template.sort.set("data",{column:new FcUtils().cleanFieldName(data.tableViewConfig.columns[data.order[0]].data),"order":order});
          }

        }
      }
        var limit = template.pageLimit.get("count");
        var skipCount = template.skipCount.get("count") * limit;
        console.log(template.selector.get("selectorQuery"));
        template.subscribe("fcTable_getInfo",tableName,collectionName,template.selector.get("selectorQuery"),sort,skipCount,limit);
    });

    template.autorun(function () {

        records = SmtAdminTabular.getRecord(tableName);
        if(_.isEmpty(records)){
            return;
        }
        template.subscribe(publicationName,collectionName,fields,records.ids);
    })*/


});

Template.FcUITableView.helpers({
   getTableTitle: function () {
       return this.table.tableViewConfig.columns;
   },
    getStaticTableData: function (){
        var tableData = [];
        //tableData = [{createdAt: new Date(),_id:"abc",username:"abc",isActive:true}];
        var collectionData = Template.instance().tableData.get("data");
        var columns = this.table.tableViewConfig.columns;
        _.each(collectionData,function (data) {
            var columnData = [];
            _.each(columns,function (fieldName) {
                if(fieldName.render != undefined){

                    columnData.push(fieldName.render(objectPath.get(data,fieldName.data),"display",data));
                }
                else if(fieldName.tmpl){
                    columnData.push({"templateName":fieldName.tmpl,data:data});
                } else if(fieldName.data && fieldName.data != ""){
                    columnData.push(objectPath.get(data,fieldName.data));
                }

            });

            tableData.push({"columnData":columnData,collectionData:data});
        });
        return tableData;
    },
    isAccordian: function (data) {
        if(data && data.table.tableViewConfig && data.table.tableViewConfig.isAccordian){
            return data.table.tableViewConfig.accordianTemplate;
        }
    },
    extraAtts: function (data) {
        if(data && data.table.tableViewConfig && data.table.tableViewConfig.isAccordian){
            return "collapse";
        }
        return "";
    },
    needFilter:function () {
        return this.isFilter?this.isFilter : false;
    },
    moreResults: function () {
        var query = getQuery(Template.instance());
        if(Template.instance().skipCount.get("count") == 0){
            if(SmtAdminTabular.getRecord(this.table.name)){
                return SmtAdminTabular.getRecord(this.table.name).recordsTotal > Template.instance().pageLimit.get("count") ? "" : "disabled"
            }

        }else{
            return Template.instance().skipCount.get("count") * Template.instance().pageLimit.get("count") < (SmtAdminTabular.getRecord(this.table.name).recordsTotal - Template.instance().pageLimit.get("count") ) ? "" : "disabled";
        }
        return false;

    },
    lessResults: function () {
        return Template.instance().skipCount.get("count") != 0 ? "" : "disabled";
    },
    checkSortingOfColumn: function(columnData){
      if(columnData){
        var sortData = Template.instance().sort.get("data");
        if(sortData){
          if(sortData.column == new FcUtils().cleanFieldName(columnData.data)){
            return sortData.order == -1 ? "fa-sort-desc" : "fa-sort-asc"
          }
        }
        return "fa-sort";
      }
    },
    checkSortingOfColumnSorted: function (columnData) {
      if(columnData){
        var sortData = Template.instance().sort.get("data");
        if(sortData){
          if(sortData.column == new FcUtils().cleanFieldName(columnData.data)){
            return sortData.order == -1 ? "desc" : "asc";
          }
        }
        return "no";
      }
    },
    getPagination: function (){
     /* var records = SmtAdminTabular.getRecord(this.table.name);
      var pages = [];
      if(_.isEmpty(records)){
          return;
      }
        var limit = Template.instance().pageLimit.get("count");
        var currentCount = Number(Template.instance().skipCount.get("count") + 1);

        return renderControls(currentCount,Math.ceil(records.recordsTotal / limit),3);*/
     return [1,2,3,4,5,6,7,8,9,10]
    },
    currentPageNumberClass: function (pageNumber) {
        var skipCount = Template.instance().skipCount.get("count");
        if(pageNumber == (skipCount + 1)){
            return "active";
        }
    },
    getCurrentPageLimit: function () {
        return Template.instance().pageLimit.get("count");
    }
});

Template.FcUITableView.events({
    "click .alphabetChar": function (e,t) {
        e.preventDefault();
        var searchString = this[0];
        var selector =  t.data.table.tableViewConfig.selector;
        if(!$(e.currentTarget).hasClass("active")){
            $(e.currentTarget).closest("ul").find(".active").removeClass("active");
            $(e.currentTarget).addClass("active");
            var filterFields = t.data.filterFields;
            var searches = [];
            if(_.isArray(filterFields) && filterFields.length > 0){
                _.each(filterFields, function(field) {
                    var m1 = {}, m2 = {};

                    // String search
                    m1[field] = { $regex: "^"+searchString};
                    m1[field].$options = "i";

                    searches.push(m1);
                });
            }
        }else{
            $(e.currentTarget).removeClass("active");
        }
        var result = {};
        if(searches && searches.length > 0){
            if (selector) {
                result = {$and: [selector, {$or:searches}]};
            } else {
                result = {$or: searches};
            }
        }else{
            if (selector) {
                result = selector;
            }
        }
        t.selector.set("selectorQuery",result);
        t.pageLimit.set("count",PAGE_LIMIT);
        t.skipCount.set("count",0);

    },
   "click #next": function (e,t) {
        e.preventDefault();
        var status = Template.instance().skipCount.get("count") * Template.instance().pageLimit.get("count") < (SmtAdminTabular.getRecord(this.table.name).recordsTotal - Template.instance().pageLimit.get("count") );
        if(status){
            var nextPage = t.skipCount.get("count");
            t.skipCount.set("count",nextPage + 1);
        }

   },
    "click #previous": function (e,t) {
        e.preventDefault();
        if(t.skipCount.get("count") != 0){
            var nextPage = t.skipCount.get("count");
            t.skipCount.set("count",nextPage - 1);
        }

    },
    "click #first": function (e,t) {
        e.preventDefault();
        if(t.skipCount.get("count") != 0){
            var nextPage = t.skipCount.get("count");
            t.skipCount.set("count",0);
        }
    },
    "click #last": function (e,t) {
        e.preventDefault();
        var records = SmtAdminTabular.getRecord(this.table.name);
        var pages = [];
        if(_.isEmpty(records)){
            return;
        }

        var limit = t.pageLimit.get("count");
        var count = Math.ceil(records.recordsTotal / limit) - 1;
        if(t.skipCount.get("count") != count){
            t.skipCount.set("count",count);
        }
    },
    "click .sortColumn": function(e,t){
      e.preventDefault();
      var currentSort = $(e.currentTarget).attr("data-sorted");
      if(currentSort == "no"){
        Router.go(Router.current().route.getName(),{},{query:{sort:this.data,order:"desc"}})
      }else{
        var order = null;
        if(currentSort == "desc"){
          order = "asc";
        }else if(currentSort == "asc"){
          order = "desc";
        }
        Router.go(Router.current().route.getName(),{},{query:{sort:this.data,order:order}});
      }
    },
    "click .paginationNumber": function(e,t) {
      e.preventDefault();
      var pageNumber = $(e.currentTarget).attr("data-page");
      console.log(pageNumber);
      var finalPageNumber = Number(Number(pageNumber) - 1);
      t.skipCount.set("count",finalPageNumber);
    },
    "change .selectPageLimit": function (e,t) {
        e.preventDefault();
        var value = $(e.currentTarget).val();
        t.pageLimit.set("count",Number(value));
        t.skipCount.set("count",0);
    }
});
function getQuery(templateInstance) {
    var query = {};
    var selector = templateInstance.selector.get("selectorQuery");
    if(selector && !_.isEmpty(selector)){
        query = selector;
    }
    return query;
}

var renderControls = function(currentPage, numPages, pagesCutoff) {
    var prevPosition = currentPage - pagesCutoff;
    var nextPosition = currentPage + pagesCutoff;
    var pagingControls = [];
    for (var i = 1; i <= numPages; i++) {
        if(i >= prevPosition && i <= nextPosition) {
            pagingControls.push(i);
        }
    }
    return pagingControls;
}