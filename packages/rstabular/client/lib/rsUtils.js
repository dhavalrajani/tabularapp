PAGE_LIMIT = 10;
PAGE_LIMIT_MR =10;
var RsUtils = function () {
    var RsUtilsClass = function () {

    };

    RsUtilsClass.prototype.cleanFieldName = function (field) {
        // for field names with a dot, we just need
        // the top level field name
        var dot = field.indexOf(".");
        if (dot !== -1) {
            field = field.slice(0, dot);
        }

        // If it's referencing an array, strip off the brackets
        field = field.split('[')[0];

        return field;
    };

    return RsUtilsClass;
};

export default RsUtils();