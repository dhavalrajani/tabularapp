Package.describe({
  name: 'rstabular',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom(['METEOR@1.3']);
  api.use('ecmascript');
    api.use([
        'check',
        'ecmascript',
        'underscore',
        'mongo',
        'blaze',
        'templating',
        'reactive-var',
        'tracker',
        'session',
        'templating',
        'fortawesome:fontawesome'
    ]);
    api.use(['jquery'], 'client', {weak: true});
    api.addFiles('client/stylesheets/tabular.css', 'client');

    api.mainModule('server/rsTabularPub.js', 'server');
    api.addFiles('client/lib/rsUtils.js', 'client');
    api.mainModule('client/rsTabular.html', 'client');
    api.mainModule('client/rsTabular.js', 'client');




});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('rstabular');
  api.mainModule('rstabular-tests.js');
});
