Meteor.publish("rsTabularView_genericPub", function (tableName,fields,ids) {
    var self = this;
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));
    if(fields && fields.length > 0){
        return global["FcCollections"][tableName.split(".")[1]].find({_id:{$in:ids}},{fields: fields});
    }else{
        return global["FcCollections"][tableName.split(".")[1]].find({_id:{$in:ids}});
    }




});

Meteor.publish("rsTable_getInfo", function(tableName,collectionName, selector, sort, skip, limit) {
    var self = this;
    check(tableName, String);
    check(collectionName,String);
    check(selector, Match.Optional(Match.OneOf(Object, null)));
    check(sort, Match.Optional(Match.OneOf(Object, null)));
    check(skip, Number);
    check(limit, Match.Optional(Match.OneOf(Number, null)));
    var condition = {};
    if(selector){
        condition = selector
    }

    skip = skip || 0;
    var findOptions = {
        skip: skip,
        fields: {_id: 1}
    };

    // `limit` may be `null`
    if (limit > 0) {
        findOptions.limit = limit;
    }

    // `sort` may be `null`
    if (sort != null) {
        findOptions.sort = sort;
    }else{
        findOptions.sort = {"audit.createdAt":-1};
    }

    var filteredCursor = global["FcCollections"][collectionName.split(".")[1]].find(condition, findOptions);

    var filteredRecordIds = filteredCursor.map(function (doc) {
        return doc._id;
    });

    var countCursor = global["FcCollections"][collectionName.split(".")[1]].find(condition, {fields: {_id: 1}});

    var recordReady = false;
    function updateRecords() {
        var currentCount = countCursor.count();

        var record = {
            ids: filteredRecordIds,
            // count() will give us the updated total count
            // every time. It does not take the find options
            // limit into account.
            recordsTotal: currentCount,
            recordsFiltered: currentCount
        };

        if (recordReady) {
            //globalLogger.info("changed", tableName, record);
            self.changed('FcTabular_records', tableName, record);
        } else {
            //globalLogger.info("added", tableName, record);
            self.added("FcTabular_records", tableName, record);
            recordReady = true;
        }
    }

    // Handle docs being added or removed from the result set.
    var initializing1 = true;
    var handle1 = filteredCursor.observeChanges({
        added: function (id) {
            if (initializing1) {
                return;
            }
            filteredRecordIds.push(id);
            updateRecords();
        },
        removed: function (id) {
            filteredRecordIds = _.without(filteredRecordIds, id);
            updateRecords();
        }
    });
    initializing1 = false;

    // Handle docs being added or removed from the non-limited set.
    // This allows us to get total count available.
    var initializing2 = true;
    var handle2 = countCursor.observeChanges({
        added: function () {
            if (initializing2) {
                return;
            }
            updateRecords();
        },
        removed: function () {
            updateRecords();
        }
    });
    initializing2 = false;

    updateRecords();
    self.ready();

    // Stop observing the cursors when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handle1.stop();
    });
    handle2.stop();

});
