// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by rstabular.js.
import { name as packageName } from "meteor/rstabular";

// Write your tests here!
// Here is an example.
Tinytest.add('rstabular - example', function (test) {
  test.equal(packageName, "rstabular");
});
